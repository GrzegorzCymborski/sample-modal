import { ApolloClient, InMemoryCache } from '@apollo/client';

const client = new ApolloClient({
  cache: new InMemoryCache({}),
  uri: 'https://be-nf.90seconds.team/workflow/graphql',
});

export { client };