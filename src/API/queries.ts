import { gql } from '@apollo/client';

export const LOGIN_MUTATION = gql`
  mutation signInMutation($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      token
    }
  }
`;

//! not connected to API

export const SIGNUP_MUTATION = gql`
  mutation signupMutation($name: String!, $phone: String!, $email: String!, $password: String!, $terms: Boolean!) {
    signup(name: $name, phone: $phone, email: $email, password: $password, terms: $terms) {
      token
    }
  }
`;

export const EMAIL_QUERY = gql`
  query emailquery($email: String!) {
    userEmail(email: $email) {
      id
    }
  }
`;

export const AGENCY_UPDATE = gql`
  mutation CompanyCreateMutation(
    $agencyName: String!
    $phone: String!
    $email: String!
    $country: String!
    $city: String!
    $terms: Boolean!
    $signUpToken: String!
  ) {
    updateAgency(
      agencyName: $agencyName
      phone: $phone
      email: $email
      country: $country
      city: $city
      terms: $terms
      signUpToken: $signUpToken
    ) {
      token
    }
  }
`;
export const BRAND_SIGNUP = gql`
  mutation brandSignupMutation(
    $brandName: String!
    $location: String!
    $email: String!
    $password: String!
    $terms: Boolean!
  ) {
    brandSignUp(brandName: $brandName, location: $location, email: $email, password: $password, terms: $terms) {
      token
    }
  }
`;

export const JOIN_BRAND = gql`
  mutation brandJoinMutation($adminEmail: String!, $name: String!, $email: String!) {
    brandSignUp(adminEmail: $adminEmail, name: $name, email: $email) {
      id
    }
  }
`;
