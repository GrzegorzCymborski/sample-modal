import { useMutation } from '@apollo/client';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import { Button, FormControl, InputGroup, Form } from 'react-bootstrap';
import { JOIN_BRAND } from '../../../API/queries';
import { joinBrandSchema } from '../../../yup/joinBrand';
import ErrorAlert from '../../errorAlert/ErrorAlert';
import modalTypes from '../modalTypes';

type JoinBrandProps = {
  onHide: () => void;
  modalType: (type: string) => void;
};

type HandleSubmitProps = {
  adminEmail: string;
  name: string;
  email: string;
};

const JoinBrand = ({ onHide, modalType }: JoinBrandProps) => {
  const [isError, setIsError] = useState(false);
  const [brandJoinMutation] = useMutation(JOIN_BRAND);

  const { touched, errors, handleChange, handleSubmit, values, resetForm, handleBlur } = useFormik({
    initialValues: {
      adminEmail: '',
      name: '',
      email: ''
    },
    onSubmit: (values) => handleSubmitForm(values),
    validationSchema: joinBrandSchema,
    validateOnChange: false
  });

  const handleSubmitForm = async (values: HandleSubmitProps) => {
    const { adminEmail, name, email } = values;
    try {
      await brandJoinMutation({
        variables: {
          adminEmail,
          name,
          email
        }
      });
      onHide();
    } catch (error) {
      setIsError(true);
      resetForm();
    }
  };

  return (
    <>
      <ErrorAlert show={isError} heading="Something went wrong..." description="Please check email or password" />
      <Form onSubmit={handleSubmit}>
        <label htmlFor="adminEmail">Brand administrator's email</label>
        <InputGroup className="mb-3">
          <FormControl
            id="adminEmail"
            name="adminEmail"
            value={values.adminEmail}
            onChange={handleChange}
            isValid={!!(touched.adminEmail && !errors.adminEmail)}
            isInvalid={!!(touched.adminEmail && errors.adminEmail)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="name">Full name</label>
        <InputGroup className="mb-3">
          <FormControl
            id="name"
            name="name"
            type="name"
            value={values.name}
            onChange={handleChange}
            isValid={!!(touched.name && !errors.name)}
            isInvalid={!!(touched.name && errors.name)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="email">Email</label>
        <InputGroup className="mb-3">
          <FormControl
            id="email"
            name="email"
            type="email"
            value={values.email}
            onChange={handleChange}
            isValid={!!(touched.email && !errors.email)}
            isInvalid={!!(touched.email && errors.email)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <Button variant="primary" block type="submit">
          Request to join
        </Button>
        <p className="text-center my-2">
          <span onClick={() => modalType(modalTypes.BRAND_SIGN_IN)} style={{ cursor: 'pointer' }}>
            Back to previous screen
          </span>
        </p>
      </Form>
    </>
  );
};

export default JoinBrand;
