import { useMutation } from '@apollo/client';
import { useFormik } from 'formik';
import React, { useState } from 'react';
import { Button, FormControl, InputGroup, Form } from 'react-bootstrap';
import { BRAND_SIGNUP } from '../../../API/queries';
import { brandSignInSchema } from '../../../yup/brandSignIn';
import ErrorAlert from '../../errorAlert/ErrorAlert';
import modalTypes from '../modalTypes';

type BrandSignInProps = {
  modalType: (type: string) => void;
  onHide: () => void;
};

type HandleSubmitProps = {
  brandName: string;
  location: string;
  email: string;
  password: string;
  checkbox: boolean;
};

const BrandSignIn = ({ onHide, modalType }: BrandSignInProps) => {
  const [isError, setIsError] = useState(false);
  const [brandSignupMutation] = useMutation(BRAND_SIGNUP);

  const { touched, errors, handleChange, handleSubmit, values, handleBlur } = useFormik({
    initialValues: {
      brandName: '',
      location: '',
      email: '',
      password: '',
      checkbox: false
    },
    onSubmit: (values) => handleSubmitForm(values),
    validationSchema: brandSignInSchema,
    validateOnChange: false
  });

  const handleSubmitForm = async (values: HandleSubmitProps) => {
    try {
      const { brandName, location, email, password, checkbox } = values;
      await brandSignupMutation({
        variables: {
          brandName,
          location,
          email,
          password,
          terms: checkbox
        }
      });
      onHide();
    } catch (error) {
      setIsError(true);
    }
  };

  return (
    <>
      <ErrorAlert show={isError} heading="Something went wrong..." description="Please check email or password" />
      <Form onSubmit={handleSubmit}>
        <label htmlFor="brandName">Brand name</label>
        <InputGroup className="mb-3">
          <FormControl
            id="brandName"
            name="brandName"
            value={values.brandName}
            onChange={handleChange}
            isValid={!!(touched.brandName && !errors.brandName)}
            isInvalid={!!(touched.brandName && errors.brandName)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="location">Location</label>
        <InputGroup className="mb-3">
          <FormControl
            id="location"
            name="location"
            type="location"
            value={values.location}
            onChange={handleChange}
            isValid={!!(touched.location && !errors.location)}
            isInvalid={!!(touched.location && errors.location)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <p
          className="text-right"
          style={{ fontSize: '12px', borderBottom: '1px solid lightgray', paddingBottom: '10px' }}
        >
          Joining an existing brand?
          <strong onClick={() => modalType(modalTypes.BRAND_JOIN)} style={{ cursor: 'pointer' }}>
            {' '}
            Request to join
          </strong>
        </p>
        <label htmlFor="email">Email</label>
        <InputGroup className="mb-3">
          <FormControl
            id="email"
            name="email"
            type="email"
            value={values.email}
            onChange={handleChange}
            isValid={!!(touched.email && !errors.email)}
            isInvalid={!!(touched.email && errors.email)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="password">Password</label>
        <InputGroup className="mb-3">
          <FormControl
            id="password"
            name="password"
            type="password"
            value={values.password}
            onChange={handleChange}
            isValid={!!(touched.password && !errors.password)}
            isInvalid={!!(touched.password && errors.password)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <Form.Group controlId="checkbox">
          <Form.Check
            type="checkbox"
            label="I have read the 90Seconds Terms & Conditions"
            value={values.checkbox as any}
            onChange={handleChange}
            isInvalid={!!(!values.checkbox && touched.checkbox)}
            onBlur={handleBlur}
          />
        </Form.Group>
        <Button variant="primary" block type="submit">
          Sign In
        </Button>
        <p className="text-center my-2">
          Already have an account?{' '}
          <span onClick={() => modalType(modalTypes.SIGN_IN)} style={{ cursor: 'pointer' }}>
            Log in
          </span>
        </p>
      </Form>
    </>
  );
};

export default BrandSignIn;
