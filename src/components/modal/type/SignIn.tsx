import { useMutation } from '@apollo/client';
import { useFormik } from 'formik';
import React, { useEffect, useState } from 'react';
import { Button, FormControl, InputGroup, Form } from 'react-bootstrap';
import { LOGIN_MUTATION } from '../../../API/queries';
import { signInSchema } from '../../../yup/signIn';
import ErrorAlert from '../../errorAlert/ErrorAlert';
import modalTypes from '../modalTypes';

type SignUpProps = {
  onHide: () => void;
  modalType: (type: string) => void;
};

type HandleSubmitProps = {
  email: string;
  password: string;
};

const SignUp = ({ onHide, modalType }: SignUpProps) => {
  const [isError, setIsError] = useState(false);
  const [signInMutation, { data }] = useMutation(LOGIN_MUTATION);

  const { touched, errors, handleChange, handleSubmit, values, resetForm, handleBlur } = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    onSubmit: (values) => handleSubmitForm(values),
    validationSchema: signInSchema,
    validateOnChange: false
  });

  const handleSubmitForm = async (values: HandleSubmitProps) => {
    const { email, password } = values;
    try {
      await signInMutation({
        variables: {
          email,
          password
        }
      });
      onHide();
    } catch (error) {
      setIsError(true);
      resetForm();
    }
  };

  useEffect(() => {
    resetForm();
    if (data) {
      console.log('user-token', data.signIn.token);
      sessionStorage.setItem('userToken', data.signIn.token);
    }
  }, [onHide, data]);

  return (
    <>
      <ErrorAlert show={isError} heading="Something went wrong..." description="Please check email or password" />
      <Form onSubmit={handleSubmit}>
        <label htmlFor="email">Email address</label>
        <InputGroup className="mb-3">
          <FormControl
            id="email"
            name="email"
            value={values.email}
            onChange={handleChange}
            isValid={!!(touched.email && !errors.email)}
            isInvalid={!!(touched.email && errors.email)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="password">Password</label>
        <InputGroup className="mb-3">
          <FormControl
            id="password"
            name="password"
            type="password"
            value={values.password}
            onChange={handleChange}
            isValid={!!(touched.password && !errors.password)}
            isInvalid={!!(touched.password && errors.password)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <Button variant="primary" block type="submit">
          Sign In
        </Button>
        <p className="text-center my-2">
          Register new{' '}
          <span onClick={() => modalType(modalTypes.SIGN_UP)} style={{ cursor: 'pointer' }}>
            account ?
          </span>
        </p>
      </Form>
    </>
  );
};

export default SignUp;
