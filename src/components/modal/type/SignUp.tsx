import { useMutation } from '@apollo/client';
import { useFormik } from 'formik';
import React, { useEffect } from 'react';
import { Button, FormControl, InputGroup, Form } from 'react-bootstrap';
import { SIGNUP_MUTATION } from '../../../API/queries';
import { signUpSchema } from '../../../yup/signUp';
import UserAvatar from '../../userAvatar/UserAvatar';
import modalTypes from '../modalTypes';

type SignUpProps = {
  onHide: () => void;
  modalType: (type: string) => void;
};

type HandleSubmitProps = {
  name: string;
  phone: string;
  email: string;
  password: string;
  checkbox: boolean;
};

const SignUp = ({ onHide, modalType }: SignUpProps) => {
  const [signupMutation, { data }] = useMutation(SIGNUP_MUTATION);

  const { touched, errors, handleChange, handleSubmit, values, handleBlur } = useFormik({
    initialValues: {
      name: '',
      phone: '',
      email: '',
      password: '',
      checkbox: false
    },
    onSubmit: (values) => handleSubmitForm(values),
    validationSchema: signUpSchema,
    validateOnChange: false
  });

  const handleSubmitForm = (values: HandleSubmitProps) => {
    // const { email, name, password, phone, checkbox } = values;
    try {
      // signupMutation({
      //   variables: {
      //     name,
      //     phone,
      //     email,
      //     password,
      //     terms: checkbox
      //   }
      // });
      modalType(modalTypes.CREATOR_ACCOUNT);
    } catch (error) {
      console.log('signup error', error);
    }
  };

  // useEffect(() => {
  //   if (data) {
  //     console.log('userToken', data.signIn.token);
  //     sessionStorage.setItem('userToken', data.signIn.token);
  //   }
  // }, [data]);

  return (
    <>
      <UserAvatar />
      <Form onSubmit={handleSubmit}>
        <label htmlFor="name">Full name</label>
        <InputGroup className="mb-3">
          <FormControl
            id="name"
            name="name"
            value={values.name}
            onChange={handleChange}
            isValid={!!(touched.name && !errors.name)}
            isInvalid={!!(touched.name && errors.name)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="phone">Phone number</label>
        <InputGroup className="mb-3">
          <FormControl
            id="phone"
            name="phone"
            type="number"
            value={values.phone}
            onChange={handleChange}
            isValid={!!(touched.phone && !errors.phone)}
            isInvalid={!!(touched.phone && errors.phone)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="email">Email</label>
        <InputGroup className="mb-3">
          <FormControl
            id="email"
            name="email"
            value={values.email}
            onChange={handleChange}
            isValid={!!(touched.email && !errors.email)}
            isInvalid={!!(touched.email && errors.email)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="password">Password</label>
        <InputGroup className="mb-3">
          <FormControl
            id="password"
            name="password"
            type="password"
            value={values.password}
            onChange={handleChange}
            isValid={!!(touched.password && !errors.password)}
            isInvalid={!!(touched.password && errors.password)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <Form.Group controlId="checkbox">
          <Form.Check
            type="checkbox"
            label="I have read the 90Seconds Terms & Conditions"
            value={values.checkbox as any}
            onChange={handleChange}
            isInvalid={!!(!values.checkbox && touched.checkbox)}
            onBlur={handleBlur}
          />
        </Form.Group>
        <Button variant="primary" block type="submit">
          Sign up
        </Button>
        <p className="text-center my-2">
          Already have an account?{' '}
          <span onClick={() => modalType(modalTypes.SIGN_IN)} style={{ cursor: 'pointer' }}>
            Log in
          </span>
        </p>
      </Form>
    </>
  );
};

export default SignUp;
