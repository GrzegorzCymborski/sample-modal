import { useMutation } from '@apollo/client';
import { useFormik } from 'formik';
import React, { useEffect, useState } from 'react';
import { Button, FormControl, InputGroup, Form, Col, Row } from 'react-bootstrap';
import { AGENCY_UPDATE } from '../../../API/queries';
import { cratorAccountSchema } from '../../../yup/creatorAccount';
import ErrorAlert from '../../errorAlert/ErrorAlert';
import UserAvatar from '../../userAvatar/UserAvatar';

type CreateCompanyAccountProps = {
  onHide: () => void;
  modalType: (type: string) => void;
};

type HandleSubmitProps = {
  agencyName: string;
  phone: string;
  email: string;
  country: string;
  city: string;
  checkbox: boolean;
};

const CreateCompanyAccount = ({ onHide, modalType }: CreateCompanyAccountProps) => {
  const [isError, setIsError] = useState(false);
  const [signUpToken, setSignUpToken] = useState('');
  const [CompanyCreateMutation] = useMutation(AGENCY_UPDATE);

  const { touched, errors, handleChange, handleSubmit, values, handleBlur } = useFormik({
    initialValues: {
      agencyName: '',
      phone: '',
      email: '',
      country: '',
      city: '',
      checkbox: false
    },
    onSubmit: (values) => handleSubmitForm(values),
    validationSchema: cratorAccountSchema,
    validateOnChange: false
  });

  const handleSubmitForm = async (values: HandleSubmitProps) => {
    try {
      const { agencyName, phone, email, country, city, checkbox } = values;
      await CompanyCreateMutation({
        variables: {
          agencyName,
          phone,
          email,
          country,
          city,
          terms: checkbox,
          signUpToken
        }
      });
      onHide();
    } catch (error) {
      setIsError(true);
      console.log('error!', error);
    }
  };

  useEffect(() => {
    setSignUpToken(sessionStorage.getItem('userToken')!);
  }, []);

  return (
    <>
      <ErrorAlert show={isError} heading="Something went wrong..." description="Please check email or password" />
      <UserAvatar />
      <Form onSubmit={handleSubmit}>
        <label htmlFor="agencyName">Agency name</label>
        <InputGroup className="mb-3">
          <FormControl
            id="agencyName"
            name="agencyName"
            value={values.agencyName}
            onChange={handleChange}
            isValid={!!(touched.agencyName && !errors.agencyName)}
            isInvalid={!!(touched.agencyName && errors.agencyName)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="phone">Phone number</label>
        <InputGroup className="mb-3">
          <FormControl
            id="phone"
            name="phone"
            type="number"
            value={values.phone}
            onChange={handleChange}
            isValid={!!(touched.phone && !errors.phone)}
            isInvalid={!!(touched.phone && errors.phone)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <label htmlFor="email">Email</label>
        <InputGroup className="mb-3">
          <FormControl
            id="email"
            name="email"
            value={values.email}
            onChange={handleChange}
            isValid={!!(touched.email && !errors.email)}
            isInvalid={!!(touched.email && errors.email)}
            onBlur={handleBlur}
          />
        </InputGroup>
        <Row>
          <Col xs={6}>
            <label htmlFor="country">Country</label>
            <InputGroup className="mb-3">
              <FormControl
                id="country"
                name="country"
                value={values.country}
                onChange={handleChange}
                isValid={!!(touched.country && !errors.country)}
                isInvalid={!!(touched.country && errors.country)}
                onBlur={handleBlur}
              />
            </InputGroup>
          </Col>
          <Col xs={6}>
            <label htmlFor="city">City</label>
            <InputGroup className="mb-3">
              <FormControl
                id="city"
                name="city"
                value={values.city}
                onChange={handleChange}
                isValid={!!(touched.city && !errors.city)}
                isInvalid={!!(touched.city && errors.city)}
                onBlur={handleBlur}
              />
            </InputGroup>
          </Col>
        </Row>
        <Form.Group controlId="checkbox">
          <Form.Check
            type="checkbox"
            label="I have read the 90Seconds Terms & Conditions"
            value={values.checkbox as any}
            onChange={handleChange}
            isInvalid={!!(!values.checkbox && touched.checkbox)}
            onBlur={handleBlur}
          />
        </Form.Group>
        <Button variant="primary" block type="submit">
          Create Company Account
        </Button>
        <p className="text-center my-2">
          Already have an account?{' '}
          <span onClick={() => modalType('signIn')} style={{ cursor: 'pointer' }}>
            Log in
          </span>
        </p>
      </Form>
    </>
  );
};

export default CreateCompanyAccount;
