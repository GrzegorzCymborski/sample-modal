const SIGN_IN = 'signIn';
const SIGN_UP = 'signUp';
const CREATOR_ACCOUNT = 'creator';
const BRAND_SIGN_IN = 'brandSignIn';
const BRAND_JOIN = 'joinBrand';

export default {
  SIGN_IN,
  SIGN_UP,
  CREATOR_ACCOUNT,
  BRAND_SIGN_IN,
  BRAND_JOIN
};
