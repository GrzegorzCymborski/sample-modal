import React from 'react';
import { Modal } from 'react-bootstrap';
import CreateCompanyAccount from './type/CreateCompanyAccount';
import SignIn from './type/SignIn';
import SignUp from './type/SignUp';
import modalTypes from './modalTypes';
import BrandSignIn from './type/BrandSignIn';
import JoinBrand from './type/JoinBrand';

type MainModalProps = {
  show: boolean;
  onHide: () => void;
  modalType: string;
  setModalType: (arg: string) => void;
};

const MainModal = ({ show, onHide, modalType, setModalType }: MainModalProps) => {
  let modalBody: JSX.Element | undefined;

  switch (modalType) {
    case modalTypes.SIGN_IN:
      modalBody = <SignIn onHide={onHide} modalType={setModalType} />;
      break;
    case modalTypes.CREATOR_ACCOUNT:
      modalBody = <CreateCompanyAccount onHide={onHide} modalType={setModalType} />;
      break;
    case modalTypes.SIGN_UP:
      modalBody = <SignUp onHide={onHide} modalType={setModalType} />;
      break;
    case modalTypes.BRAND_SIGN_IN:
      modalBody = <BrandSignIn onHide={onHide} modalType={setModalType} />;
      break;
    case modalTypes.BRAND_JOIN:
      modalBody = <JoinBrand onHide={onHide} modalType={setModalType} />;
      break;
    default:
      modalBody = <SignUp onHide={onHide} modalType={setModalType} />;
  }

  return (
    <Modal show={show} onHide={onHide} centered>
      <Modal.Body className="d-flex flex-column">
        <button
          className="btn-link btn w-5 align-self-end text-decoration-none text-dark"
          onClick={onHide}
          style={{ cursor: 'pointer' }}
        >
          Close
        </button>
        {modalBody}
      </Modal.Body>
    </Modal>
  );
};

export default MainModal;
