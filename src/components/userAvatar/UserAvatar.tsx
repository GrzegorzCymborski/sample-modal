import React, { useState } from 'react';
import { Row, Col, Image } from 'react-bootstrap';

const UserAvatar = () => {
  const [previewImg, setPreviewImg] = useState('');
  return (
    <Row>
      <Col className="d-flex my-4">
        <input
          accept="image/png, image/jpeg"
          type="file"
          id="files"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPreviewImg(URL.createObjectURL(e.target.files![0]))}
          style={{ display: 'none' }}
        />
        <Image src={previewImg || 'https://picsum.photos/200'} roundedCircle height="80px" className="mr-3" />
        <div>
          <h6>Add a profile photo</h6>
          <p>Your profile picture is an important element of your 90seconds presence</p>
          <label htmlFor="files" className="text-primary">
            {previewImg ? 'Change photo' : 'Upload photo'}
          </label>
        </div>
      </Col>
    </Row>
  );
};

export default UserAvatar;
