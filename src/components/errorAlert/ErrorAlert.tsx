import React from 'react';
import { Alert } from 'react-bootstrap';

type ErrorAlerProps = {
  show: boolean;
  heading: string;
  description: string;
};

const ErrorAlert = ({ show, heading, description }: ErrorAlerProps) => {
  return (
    <Alert variant="danger" show={show}>
      <Alert.Heading>{heading}</Alert.Heading>
      <p>{description}</p>
    </Alert>
  );
};

export default ErrorAlert;
