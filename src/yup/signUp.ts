import * as yup from 'yup';
import fakeEmailResponse from '../API/fakeResponses/emailCheck.json';

export const signUpSchema = yup.object({
  name: yup.string().required('Name is required ').min(5, 'Minimum 5 chars').max(25, 'Max 25 chars'),
  phone: yup.string().required('Phone is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  email: yup
    .string()
    .email()
    .required('Email is required ')
    .test(
      'validator',
      'email already used',
      async (value): Promise<any> => {
        if (value) {
          if (fakeEmailResponse.data.userEmail.id) {
            return true;
          }
          // const response = await fetch('https://be-nf.90seconds.team/workflow/graphql', {
          //   method: 'POST',
          //   headers: {
          //     'Content-Type': 'application/json'
          //   },
          //   body: JSON.stringify({
          //     query: `
          //     query emailquery($email: String!) {
          //       userEmail(email: $email) {
          //         id
          //       }
          //     }
          //     `
          //   })
          // });
          // const emailInfo = await response.json();
          // if (emailInfo.id) {
          //   return true;
          // }
        }
        return false;
      }
    ),
  password: yup.string().required('Password is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  checkbox: yup.bool().isTrue()
});
