import * as yup from 'yup';

export const signInSchema = yup.object({
  email: yup.string().email().required('Email is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  password: yup.string().required('Password is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars')
});
