import * as yup from 'yup';

export const joinBrandSchema = yup.object({
  adminEmail: yup.string().email().required('Administrators email is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  name: yup.string().required('Name is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  email: yup.string().email().required('Email is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars')
});
