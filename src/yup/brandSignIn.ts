import * as yup from 'yup';
import fakeBrandResponse from '../API/fakeResponses/brandCheck.json'

export const brandSignInSchema = yup.object({
  brandName: yup.string().required('Brand name is required').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars').test(
    'validator',
    'brand name already used',
    async (value): Promise<any> => {
      if (value) {
        if (fakeBrandResponse.data.brandAvailable.id) {
          return true;
        }
        // const response = await fetch('https://be-nf.90seconds.team/workflow/graphql', {
        //   method: 'POST',
        //   headers: {
        //     'Content-Type': 'application/json'
        //   },
        //   body: JSON.stringify({
        //     query: `
        //     query brandquery($brand: String!) {
        //       brandAvailable(brand: $brand) {
        //         id
        //       }
        //     }
        //     `
        //   })
        // });
        // const brandInfo = await response.json();
        // if (brandInfo.id) {
        //   return true;
        // }
      }
      return false;
    }
  ),
  location: yup.string().required('Location is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  email: yup.string().email().required('Email is required ').max(30, 'Max 30 chars'),
  password: yup.string().required('Password is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  checkbox: yup.bool().isTrue()
});
