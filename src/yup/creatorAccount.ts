import * as yup from 'yup';

export const cratorAccountSchema = yup.object({
  agencyName: yup
    .string()
    .required('Agency name is required ')
    .min(5, 'Minimum 5 chars')
    .max(30, 'Max 30 chars'),
  phone: yup.string().required('Phone is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  email: yup.string().email().required('Email is required ').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  country: yup.string().required('Country is required').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  city: yup.string().required('Country is required').min(5, 'Minimum 5 chars').max(30, 'Max 30 chars'),
  checkbox: yup.bool().isTrue()
});
