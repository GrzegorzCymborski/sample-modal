import React, { useState } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import MainModal from './components/modal/Modal';
import modalTypes from './components/modal/modalTypes';

const App = () => {
  const [showModal, setShowModal] = useState(false);
  const [modalType, setModalType] = useState('');

  const handleBrandModalShow = (type: string) => {
    setShowModal(!showModal);
    setModalType(type);
  };

  return (
    <Container className="d-flex flex-column" fluid style={{ height: '100vh' }}>
      <Card
        style={{
          height: '100%',
          background: `center / cover no-repeat url(https://picsum.photos/1920)`,
          margin: '1rem 0'
        }}
      >
        <div className="d-flex justify-content-end">
          <Button variant="primary" onClick={() => handleBrandModalShow(modalTypes.SIGN_UP)} className="m-2">
            Creator
          </Button>
          <Button variant="primary" onClick={() => handleBrandModalShow(modalTypes.BRAND_SIGN_IN)} className="m-2">
            Brand
          </Button>
        </div>
      </Card>
      <MainModal
        show={showModal}
        onHide={() => setShowModal(!setShowModal)}
        modalType={modalType}
        setModalType={setModalType}
      />
    </Container>
  );
};

export default App;
